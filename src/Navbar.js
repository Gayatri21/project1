import React from "react";
import { NavLink } from "react-router-dom";
import "./index.css";

const Navbar = () => {
  return (
    <main>
      <div className="nav-bg ">
        <div className="">
          <nav className="navbar navbar-expand-lg ">
            <div className="container ">
              <NavLink className="navbar-brand" to="#">
                <img className="img" src="../images/Logo.png" />
              </NavLink>
                            <div
                className="collapse navbar-collapse hidden"
                id="navbarSupportedContent"
              >
                <div className="mx-auto">
                  <ul className="navbar-nav px-20 mb-2 mb-lg-0 ">
                    <li className="nav-item">Home</li>
                    <li className="nav-item">Categories</li>
                    <li className="nav-item">About us</li>
                  </ul>
                </div>
                <h1 className=" d-lg-block ">
                  <button type="button" className="py-1 px-3 btn  ">
                    <img src="../images/Vector.png" alt="" className="p-2" />
                    Register now
                  </button>
                </h1>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </main>
  );
};

export default Navbar;

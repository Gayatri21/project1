import React from "react";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../node_modules/bootstrap/dist/js/bootstrap.bundle";
import Home from "./Home";
import Navbar from "./Navbar";
import { Routes , Route} from "react-router-dom";
import './index';

const App = () => {
  return (
    <>
    <Navbar/>
    <Home/>
      {/* <Routes>
        <Route exact path="/Home" element={<Home/>}/>               
      </Routes> */}     
    </>
  );
};

export default App;
